import React from 'react';
import BannerComponent from '../BannerComponent';
import BannerDescriptionComponent from '../BannerDescriptionComponent';

const BannerDetailPageLayout = () => {

  return (
    <div>
      <BannerComponent>
        <BannerDescriptionComponent />
      </BannerComponent>
    </div>
  )
}

export default BannerDetailPageLayout;
