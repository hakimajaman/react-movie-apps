import React from 'react';
import BannerComponent from '../BannerComponent';
import SliderImageComponent from '../SliderImageComponent';

const BannerHomePageLayout = () => {
  return (
    <div>
      <BannerComponent>
        <SliderImageComponent />
      </BannerComponent>
    </div>
  )
}

export default BannerHomePageLayout;
