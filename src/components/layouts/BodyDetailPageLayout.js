import React from 'react';
import BodyComponent from '../BodyComponent';
import FilterDetailPageComponent from '../FilterDetailPageComponent';
import ReviewComponent from '../ReviewComponent';
import "../../assets/sass/LayoutDetailPage.scss";

const BodyDetailPageLayout_Review = () => {
  return (
    <div>
      <BodyComponent>
        <FilterDetailPageComponent />
        <ReviewComponent/>
      </BodyComponent>
    </div>
  )
}

export default BodyDetailPageLayout_Review;
