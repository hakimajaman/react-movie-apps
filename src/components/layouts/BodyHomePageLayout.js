import React from 'react';
import BodyComponent from '../BodyComponent';
import FilterHomePageComponent from '../FilterHomePageComponent';
import BodyHomePageComponent from '../BodyHomePageComponent';
import PaginationComponent from '../PaginationComponent';

const BodyHomePageLayout = () => {
  return (
    <div>
      <BodyComponent>
        <FilterHomePageComponent />
        <BodyHomePageComponent />
        <PaginationComponent />
      </BodyComponent>
    </div>
  )
}

export default BodyHomePageLayout;
