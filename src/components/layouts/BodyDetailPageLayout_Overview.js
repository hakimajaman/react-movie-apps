import React from 'react';
import BodyComponent from '../BodyComponent';
import FilterDetailPageComponent from '../FilterDetailPageComponent';
import OverviewComponent from '../OverviewComponent';
import "../../assets/sass/LayoutDetailPage.scss";

const BodyDetailPageLayout_Overview = () => {
  return (
    <div>
      <BodyComponent>
        <FilterDetailPageComponent />
        <OverviewComponent/>
      </BodyComponent>
    </div>
  )
}

export default BodyDetailPageLayout_Overview;
