import React from 'react';
import BodyComponent from '../BodyComponent';
import FilterDetailPageComponent from '../FilterDetailPageComponent';
import CharactersComponent from '../CharactersComponent';
import "../../assets/sass/LayoutDetailPage.scss";

const BodyDetailPageLayout_Characters = () => {
  return (
    <div>
      <BodyComponent>
        <FilterDetailPageComponent />
        <CharactersComponent />
      </BodyComponent>
    </div>
  )
}

export default BodyDetailPageLayout_Characters;
