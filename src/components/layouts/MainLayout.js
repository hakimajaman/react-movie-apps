import React from 'react';
import HeaderComponent from '../HeaderComponent';
import FooterComponent from '../FooterComponent';

const MainLayout = (props) => {
  return (
    <div>
      <HeaderComponent />
        {props.children}
      <FooterComponent />
    </div>
  )
}

export default MainLayout;
