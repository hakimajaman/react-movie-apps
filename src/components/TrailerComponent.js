import React, { useEffect } from 'react';
import "../assets/sass/TrailerComponent.scss";
import { useParams } from "react-router-dom"

//Redux
import {connect} from 'react-redux';
import { getMovie } from '../store/actions/movies';

const TrailerComponent = (props) => {

  const id = useParams()

  useEffect(() => {
    props.getMovie(id.id)
  }, [])

  const onBlur = () => {
    document.getElementById('showtrailer').style.display = 'none'
  }

  return (
    <div className="trailer-container" id="showtrailer" onClick={onBlur}>
      <div className="trailer-container__box" id="blur">
        <div className="trailer-container__title">
          <p>Trailer { props.overviewMovie ? props.overviewMovie.title : ""}</p>
        </div>
        <div>
          <iframe src={props.overviewMovie ? props.overviewMovie.trailer : ""} title={ props.overviewMovie ? props.overviewMovie.title : ""}/>
        </div>
      </div>
    </div>
  )
}

const mapStateToProps = state => {
  return {
    overviewMovie: state.movieReducers.movie,
  };
};

export default connect(mapStateToProps, {
  getMovie
})( TrailerComponent );
