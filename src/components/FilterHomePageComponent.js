import React, { useState } from 'react';
import "../assets/sass/FilterComponent.scss";

//redux
import { connect } from 'react-redux';
import { searchMovies, searchCategoryMovies } from '../store/actions/movies';

const FilterHomePageComponent = (props) => {

  const [category, setCategory] = useState("")

  const change = (e) => {
    setCategory(e.target.value)
  }

  const onSearch = (e) => {
    setCategory(e)
    props.searchCategoryMovies(category)
  }

  return (
    <div className="filter-container">
      <h3>Browse by category</h3>
      <div className="filter-container__button">
        <p onClick={() => onSearch("action")}>Action</p>
        <p onClick={() => onSearch("adventure")} value="adventure">Adventure</p>
        <p onClick={() => onSearch("comedy")} value="comedy">Comedy</p>
        <p onClick={() => onSearch("romance")} value="romance">Romance</p>
        <p onClick={() => onSearch("sci-fi")} value="sci-fi">Sci-Fi</p>
      </div>
    </div>
  )
}

const mapStateToProps = (state) => {
  return({
    searchMovies: state
  })
}

export default connect(mapStateToProps, {
  searchMovies,
  searchCategoryMovies
})( FilterHomePageComponent );
