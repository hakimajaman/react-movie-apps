import React from 'react';
import "../assets/sass/BannerComponent.scss";

const BannerComponent = (props) => {
  return(
    <div className="banner-container">
      {props.children}
    </div>
  )
}

export default BannerComponent;
