import React from 'react';
import "../assets/sass/PaginationComponent.scss";

//Redux
import { connect } from 'react-redux';
import { getAllMoviesPagination } from '../store/actions/movies';

const PaginationComponent = (props) => {

  var pagin = [];

  const toPaginationNumber = (e) => {
    props.getAllMoviesPagination(e)
  }

  const allMovies = () => {
    if(props.pagination.movieReducers.movies !== undefined) {
      for( let i = 1; i<= props.pagination.movieReducers.movies.totalPages; i++){
        console.log(i)
        pagin.push(
          <div key={i}><p onClick={() => toPaginationNumber(i)}>{i}</p></div>
        )
      }
    }
  }

  return(
    <div className="body-pagination">
    {allMovies()}
      <div><span>&#x2190;</span></div>
      {pagin}
      <div><span>&#x2192;</span></div>
    </div>
  )
}

const mapStateToProps = (state) => {
  return ({
    pagination: state
  })
}

export default connect(mapStateToProps, {
  getAllMoviesPagination
})( PaginationComponent );
