import React, {useEffect} from 'react';
import { useParams} from "react-router-dom"

//Redux
import {connect} from 'react-redux';
import { getMovie } from '../store/actions/movies';


const CharactersComponent = (props) => {

  const id = useParams()

  useEffect(() => {
    props.getMovie(id.id)
  }, [])

  const images = [
    require('../assets/images/tanjiro.jpg'),
    require('../assets/images/Nezuko_anime_design.png'),
    require('../assets/images/Zenitsu_anime_design.png'),
    require('../assets/images/Shinobu_anime.png'),
    require('../assets/images/Sabito_Anime.png')
  ]

  return (
    <div className="body-container__characters">
      <div className="col-img-flex">
        <div className="col-img">
          <img src={images[0]} alt="img1" />
        </div>
        <p>Yakusoku no Neverland</p>
      </div>
      <div className="col-img-flex">
        <div className="col-img">
          <img src={images[1]} alt="img1" />
        </div>
        <p>Yakusoku no Neverland</p>
      </div>
      <div className="col-img-flex">
        <div className="col-img">
          <img src={images[2]} alt="img1" />
        </div>
        <p>Yakusoku no Neverland</p>
      </div>
      <div className="col-img-flex">
        <div className="col-img">
          <img src={images[3]} alt="img1" />
        </div>
        <p>Yakusoku no Neverland</p>
      </div>
      <div className="col-img-flex">
        <div className="col-img">
          <img src={images[4]} alt="img1" />
        </div>
        <p>Yakusoku no Neverland</p>
      </div>
      <div className="col-img-flex">
        <div className="col-img">
          <img src={images[0]} alt="img1" />
        </div>
        <p>Yakusoku no Neverland</p>
      </div>
      <div className="col-img-flex">
        <div className="col-img">
          <img src={images[1]} alt="img1" />
        </div>
        <p>Yakusoku no Neverland</p>
      </div>
      <div className="col-img-flex">
        <div className="col-img">
          <img src={images[2]} alt="img1" />
        </div>
        <p>Yakusoku no Neverland</p>
      </div>
      <div className="col-img-flex">
        <div className="col-img">
          <img src={images[3]} alt="img1" />
        </div>
        <p>Yakusoku no Neverland</p>
      </div>
      <div className="col-img-flex">
        <div className="col-img">
          <img src={images[4]} alt="img1" />
        </div>
        <p>Yakusoku no Neverland</p>
      </div>
      <div className="col-img-flex">
        <div className="col-img">
          <img src={images[3]} alt="img1" />
        </div>
        <p>Yakusoku no Neverland</p>
      </div>
      <div className="col-img-flex">
        <div className="col-img">
          <img src={images[4]} alt="img1" />
        </div>
        <p>Yakusoku no Neverland</p>
      </div>
      <div className="col-img-flex">
        <div className="col-img">
          <img src={images[3]} alt="img1" />
        </div>
        <p>Yakusoku no Neverland</p>
      </div>
      <div className="col-img-flex">
        <div className="col-img">
          <img src={images[4]} alt="img1" />
        </div>
        <p>Yakusoku no Neverland</p>
      </div>
    </div>
  )
}

const mapStateToProps = state => {
  return{
    overviewMovie: state.movieReducers.movie
  }
}

export default connect(mapStateToProps, {
  getMovie
})( CharactersComponent );
