import React, { useState, useEffect, useCallback } from 'react';
import {Link} from 'react-router-dom';
import "../assets/sass/SignComponent.scss";

//Redux
import { connect } from 'react-redux';
import { loginUser, registerUser } from '../store/actions/users';

const SignComponent = (props) => {

  const [user, setUser] = useState({
    email: "",
    password: ""
  })

  const [registUser, setRegistUser] = useState({
    name: "",
    email: "",
    password: ""
  })

  const chooseSignIn = () => {
    document.getElementById('sign-up').style.display = 'none';
    document.getElementById('sign-in').style.display = 'block';
  }

  const chooseSignUp = () => {
    document.getElementById('sign-up').style.display = 'block';
    document.getElementById('sign-in').style.display = 'none';
  }
  
  const change = (e) => {
    setUser({
      ...user,
      [e.target.name]: e.target.value
    })
  }

  const changeRegist = (e) => {
    setRegistUser({
      ...registUser,
      [e.target.name]: e.target.value
    })
  }

  const onSignIn = (e) => {
    e.preventDefault()
    props.loginUser(user.email, user.password)
  }

  const onSignUp = e => {
    e.preventDefault()
    props.registerUser(registUser.name, registUser.email, registUser.password)
  }

  return(
    <div className="form-container" id="showbox">
      <div className="form-container__box">
        <div className="logo">
          <img src={require("../assets/images/iconfinder-Video_Logo_Play_Icon.png")} alt="logo"/>
          <h3>MyAnime</h3>
        </div>
        <div id="sign-up">
          <form>
            <span>Full Name</span>
            <input type="text" name="name" value={registUser.name} onChange={changeRegist}/>
            <span>Email</span>
            <input type="email" name="email" value={registUser.email} onChange={changeRegist}/>
            <span>Password</span>
            <input type="password" name="password" value={registUser.password} onChange={changeRegist}/>
            <button onClick={onSignUp}>Sign Up</button>
          </form>
          <div>
            <span>Already have an account ?</span>
            <Link onClick={chooseSignIn}><span>Log in</span></Link>
          </div>
        </div>
        <div id="sign-in">
          <form onSubmit={onSignIn}>
            <span>Email</span>
            <input type="email" name="email" onChange={change} value={user.email}/>
            <span>Password</span>
            <input type="password" name="password" onChange={change} value={user.password}/>
            <button>Sign In</button>
          </form>
          <div>
            <span>Don't have an account ?</span>
            <Link onClick={chooseSignUp}><span>Sign up</span></Link>
          </div>
        </div>
      </div>
    </div>
  )
}

const mapStateToProps = (state) => {
  return ({
    registerUser: state
  })
}

export default connect(mapStateToProps, {
  loginUser,
  registerUser
})( SignComponent );
