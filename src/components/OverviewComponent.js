import React, { useEffect } from 'react';
import '../assets/sass/OverviewComponent.scss';
import { useParams} from "react-router-dom"

//Redux
import {connect} from 'react-redux';
import { getMovie } from '../store/actions/movies';

const OverviewComponent = (props) => {
  const id = useParams()

  useEffect(() => {
    props.getMovie(id.id)
  }, [])

  const overviewMovie = () => {
    return (
      <div>
        <div className="overview-container__description">
          <h3 className="section-heading">Synopsis</h3>
          <p>{props.overviewMovie ? props.overviewMovie.synopsis : ""}</p>
        </div>
        <div className="overview-container__description">
          <h3 className="section-heading">Movie Info</h3>
          <form>
            <div className="form">
              <p>Realese Date :</p>
              <span>{props.overviewMovie ? props.overviewMovie.releaseDate : ""}</span>
            </div>
            <div className="form">
              <p>Director :</p>
              <span>{props.overviewMovie ? props.overviewMovie.director : ""}</span>
            </div>
            <div className="form">
              <p>Budget :</p>
              <span>{props.overviewMovie ? props.overviewMovie.budget : ""}</span>
            </div>
          </form>
        </div>
      </div>
    );
  };

  return (
    <div className="overview-container">
      {overviewMovie()}
    </div>
  );
};

const mapStateToProps = state => {
  return {
    overviewMovie: state.movieReducers.movie,
  };
};

export default connect(mapStateToProps, {
  getMovie,
})(OverviewComponent);
