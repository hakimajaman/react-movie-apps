import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import "../assets/sass/HeaderComponent.scss";
import "../assets/sass/ProfilePicture.scss";
import SignComponent from './SignComponent';

//redux
import { connect } from 'react-redux';
import { searchMovies, searchTagMovies } from '../store/actions/movies';
import { currentUser } from '../store/actions/users';

const HeaderComponent = (props) => {

  let userAccount = localStorage.getItem('userAccount')
  
  useEffect(() => {
    if(!userAccount){
      document.getElementById('not-sign').style.display = 'block';
      document.getElementById('sign').style.display = 'none';
    } else {
      document.getElementById('not-sign').style.display = 'none';
      document.getElementById('sign').style.display = 'block';
    }
  })

  useEffect(() => {
    props.currentUser()
  }, [])

  const [search, setSearch] = useState("")

  const change = (e) => {
    setSearch(e.target.value)
  }

  const openBoxProfile = () => {
    if(document.getElementById('profileBox').style.display === 'flex'){
      document.getElementById('profileBox').style.display = 'none'
    } else {
      document.getElementById('profileBox').style.display = 'flex'
    }
  }

  const openBoxSign = () => {
    if(document.getElementById('showbox').style.display === 'block'){
      document.getElementById('showbox').style.display = 'none'
    } else {
      document.getElementById('showbox').style.display = 'block'
    }
  }

  const onSearch = (e) => {
    e.preventDefault()
    if(search.charAt(0) === "#"){
      props.searchTagMovies(search.substr(1))
    } else {
      props.searchMovies(search)
    }
  }

  const sign_out = () => {
    localStorage.removeItem('userAccount');
  }

  return(
    <div className="headerContainer">
      <SignComponent/>
      <div className="headerContainer__left">
        <Link to="/"><img src={require("../assets/images/iconfinder-Video_Logo_Play_Icon.png")} alt="icon-logo"/></Link>
        <Link to="/"><h3>MyAnime</h3></Link>
      </div>
      <div className="headerContainer__right">
        <form onSubmit={onSearch}>
          <input type="text" placeholder="Search Anime" id="search-component" value={search} onChange={change}/>
          <button>Search</button>
        </form>
        <div id="not-sign">
          <Link to="#" onClick={() => openBoxSign()}>Sign in</Link>
        </div>
        <div className="profile-picture-container" id="sign">
    {console.log(props)}
          <img src={props.users && props.users.status === true ? props.users.data.image : require("../assets/images/profile-picture.png")} alt="profile-pict" onClick={openBoxProfile}/>
        </div>
      </div>
      <div className="headerContainer__profileBox" id="profileBox">
        <p>{props.users && props.users.status === true ? props.users.data.name : "Nama"}</p>
        <Link to="/profile">Profile</Link>
        <Link to="#">Settings</Link>
        <Link to="#">Help</Link>
        <Link to="/" onClick={sign_out}>Sign out</Link>
      </div>
    </div>
  )
}

const mapStateToProps = (state) => {
  return({
    search: state,
    users: state.userReducers.users.payload
  })
}

export default connect(mapStateToProps, {
  searchMovies,
  searchTagMovies,
  currentUser
})( HeaderComponent );
