import React from 'react';
import {Slide} from 'react-slideshow-image';
import '../assets/sass/SliderImageComponent.scss';

const SliderImageComponent = () => {
  const slideImages = [
    require('../assets/images/yakusoku-no-neverland.jpg'),
    require('../assets/images/kimetsu-no-yaiba.jpg'),
    require('../assets/images/kimi-no-nawa.jpg'),
  ];

  const properties = {
    duration: 5000,
    transitionDuration: 500,
    infinite: true,
    indicators: true,
    arrows: false,
  };

  return (
    <div className="slide-container">
      <Slide {...properties} className="slide-container__slider">
        <div className="each-slide">
          <div>
            <img src={slideImages[0]} alt="img"/>
          </div>
        </div>
        <div className="each-slide">
          <div>
            <img src={slideImages[1]} alt="img"/>
          </div>
        </div>
        <div className="each-slide">
          <div>
            <img src={slideImages[2]} alt="img"/>
          </div>
        </div>
      </Slide>
    </div>
  );
};

export default SliderImageComponent;
