import React, { useState, useEffect } from 'react';
import "../assets/sass/ProfileComponent.scss";
import { Link } from 'react-router-dom';

//redux
import { connect } from 'react-redux';
import { currentUser, updateMyProfile, updateMyPicture } from '../store/actions/users';

const ProfileComponent = (props) => {

  const [yourName, setYourName] = useState(props.users && props.users.status === true ? props.users.data.name : "")
  const [yourPicture, setYourPicture] = useState("")

  const change = e => {
    setYourName(e.target.value)
  }

  const changePict = e => {
    setYourPicture(e.target.value)
  }

  useEffect(() => {
    props.currentUser()
  }, [])

  const onSubmit = (e) => {
    e.preventDefault()
    console.log('ueee')
    props.updateMyProfile(yourName)
    props.updateMyPicture(yourPicture)
  }

  return (
    <div className="form-profile-container" id="profilebox">
      <div>
        <h2 id="back-to-home"><Link to="/">X</Link></h2>
      </div>
      <div className="form-profile-container__box">
        <div className="logo-change">
          <div className="logo-change__picture">
            <img src={props.users && props.users.status === true ? props.users.data.image : require('../assets/images/tanjiro.jpg')} alt="profile" className="logo-change__image-image" />
          </div>
          <div className="logo-change__change">
    {
      //<input type="image" alt="imaaage" id="logo-change__icon">
    }
    <p>Change Picture</p>
    {
      //</input>
    }
          </div>
          <h3>MyProfile</h3>
        </div>
        <div id="profile">
          <form>
            <span>Update My Name</span>
            <input type="text" name="name" value={yourName} onChange={change}/>
            <button onClick={onSubmit}>Update</button>
          </form>
        </div>
      </div>
    </div>
  );
}

const mapStateToProps = (state) => {
  return({
    users: state.userReducers.users.payload
  })
}

export default connect(mapStateToProps, {
  currentUser,
  updateMyPicture,
  updateMyProfile
})( ProfileComponent );
