import React, { useState, useEffect } from 'react';
import "../assets/sass/ReviewComponent.scss";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { useParams } from "react-router-dom"

//Redux
import {connect} from 'react-redux';
import { getAllReviews, postReview } from '../store/actions/reviews';

const ReviewComponent = (props) => {
  const id = useParams()

  const [review, setReview] = useState({
    rating: 3,
    review: "",
    movieId: id.id
  })

  const change = e => {
    setReview({
      ...review,
      [e.target.name]: e.target.value
    })
  }

  useEffect(() => {
    props.getAllReviews(id.id)
  }, [])

  const images = [
    require('../assets/images/profile-picture.png'),
    require('../assets/images/tanjiro.jpg'),
    require('../assets/images/Zenitsu_anime_design.png'),
    require('../assets/images/Sabito_Anime.png'),
    require('../assets/images/Shinobu_anime.png')
  ]

  const allReviews = () => {
    if(props.reviewMovies){
      return(
        props.reviewMovies.reviews.map(item =>
          <div className="review-container__allreviews-review">
            <div>
              <div className="review-container__picture">
                <img src={item.userId.image} alt="profile" />
              </div>
            </div>
            <div className="review-container__review">
              <p>{item.userId.name}</p>
              <span>{item.review}</span>
            </div>
          </div>
        )
      )
    }
  }

  const onSubmit = (e) => {
    e.preventDefault()
    props.postReview(review.rating, review.review, review.movieId)
  }

  return (
    <div className="review-container">
      <div className="review-container__myreview">
        <div>
          <div className="review-container__picture">
            <img src={props.userss.payload ? props.userss.payload.data.image : images[1]} alt="profile" />
          </div>
        </div>
        <form>
          <div className="review-container__review">
            <p>{props.userss.payload ? props.userss.payload.data.name : "Name"}</p>
            <FontAwesomeIcon icon={["fas", "star"]} id="rev-star1"/>
            <FontAwesomeIcon icon={["fas", "star"]} id="rev-star2"/>
            <FontAwesomeIcon icon={["fas", "star"]} id="rev-star3"/>
            <FontAwesomeIcon icon={["fas", "star"]} id="rev-star4"/>
            <FontAwesomeIcon icon={["fas", "star"]} id="rev-star5"/>
          </div>
          <textarea placeholder="Leave a review" name="review" value={review.review} onChange={change}/>
          <button onClick={onSubmit}>Submit</button>
        </form>
      </div>
      <h3>All Reviews</h3>
      <div className="review-container__allreviews">
        {allReviews()}
      </div>
    </div>
  )
}

const mapStateToProps = state => {
  return {
    reviewMovies: state.reviewReducers,
    userss: state.userReducers.users
  };
};

export default connect(mapStateToProps, {
  getAllReviews,
  postReview
})( ReviewComponent );
