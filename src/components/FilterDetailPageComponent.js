import React from 'react';
import { Link, useParams } from 'react-router-dom';
import '../assets/sass/FilterComponent.scss';

const FilterDetailPageComponent = () => {
  
  const id = useParams()

  return (
    <div className="filter-container">
      <div className="filter-container__button">
        <Link to={`/list/overview/${id.id}`}><p>Overview</p></Link>
        <Link to={`/list/characters/${id.id}`}><p>Characters</p></Link>
        <Link to={`/list/review/${id.id}`}><p>Review</p></Link>
      </div>
    </div>
  );
};

export default FilterDetailPageComponent;
