import React from 'react';
import "../assets/sass/BodyComponent.scss";

const BodyComponent = (props) => {

  return (
    <div className="body-container">
      {props.children[0]}
      {props.children[1]}
      {props.children[2]}
    </div>
  )
}

export default BodyComponent;
