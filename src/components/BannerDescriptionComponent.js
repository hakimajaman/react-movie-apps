import React, {useEffect} from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import "../assets/sass/BannerDescriptionComponent.scss";
import TrailerComponent from './TrailerComponent';
import { useParams } from "react-router-dom"

//Redux
import {connect} from 'react-redux';
import { getMovie } from '../store/actions/movies';

const BannerDescriptionComponent = (props) => {

  const id = useParams()

  useEffect(() => {
    props.getMovie(id.id)
  }, [])

  const openTrailer = () => {
    document.getElementById('showtrailer').style.display = "flex"
  }

  const addWatchList = () => {
    document.getElementById('color2').style.removeProperty('background')
  }

  return (
    <div>
      <TrailerComponent/>
      <img src={props.overviewMovie ? props.overviewMovie.poster : require('../assets/images/kimetsu-no-yaiba.jpg')} alt="description-img"/>
      <div className="banner-description-container">
        <div className="banner-description-container__description">
          <h3>{props.overviewMovie ? props.overviewMovie.title : ""}</h3>
          <div className="banner-rating">
            <FontAwesomeIcon icon={["fas", "star"]} id="star1"/>
            <FontAwesomeIcon icon={["fas", "star"]} id="star2"/>
            <FontAwesomeIcon icon={["fas", "star"]} id="star3"/>
            <FontAwesomeIcon icon={["fas", "star"]} id="star4"/>
            <FontAwesomeIcon icon={["fas", "star-half-alt"]} id="star5"/>
            <p>2200 reviews</p>
          </div>
          <p>{props.overviewMovie ? props.overviewMovie.synopsis : ""}</p>
        </div>
        <div className="banner-description-container__button">
          <button id="color1" onClick={openTrailer}>Watch Trailer</button>
          <button id="color2" onClick={addWatchList}>Add to Watchlist</button>
          <button id="color3">Remove from Watchlist</button>
        </div>
      </div>
    </div>
  )
}

const mapStateToProps = state => {
  return {
    overviewMovie: state.movieReducers.movie,
  };
};

export default connect(mapStateToProps, {
  getMovie
})( BannerDescriptionComponent );
