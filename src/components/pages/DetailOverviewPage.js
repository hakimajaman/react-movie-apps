import React from 'react';
import MainLayout from '../layouts/MainLayout';
import BannerDetailPageLayout from '../layouts/BannerDetailPageLayout';
import BodyDetailPageLayoutOverview from '../layouts/BodyDetailPageLayout_Overview';

const DetailPageOverview = () => {
  return (
    <div>
      <MainLayout>
        <BannerDetailPageLayout />
        <BodyDetailPageLayoutOverview />
      </MainLayout>
    </div>
  )
}

export default DetailPageOverview;
