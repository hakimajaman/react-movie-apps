import React from 'react';
import MainLayout from '../layouts/MainLayout';
import BannerDetailPageLayout from '../layouts/BannerDetailPageLayout';
import BodyDetailPageLayoutCharacters from '../layouts/BodyDetailPageLayout_Characters';

const DetailPageCharacters = () => {
  return (
    <div>
      <MainLayout>
        <BannerDetailPageLayout />
        <BodyDetailPageLayoutCharacters/>
      </MainLayout>
    </div>
  )
}

export default DetailPageCharacters;
