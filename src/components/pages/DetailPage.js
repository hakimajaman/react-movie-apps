import React from 'react';
import MainLayout from '../layouts/MainLayout';
import BannerDetailPageLayout from '../layouts/BannerDetailPageLayout';
import BodyDetailPageLayout from '../layouts/BodyDetailPageLayout';

const DetailPage = () => {
  return (
    <div>
      <MainLayout>
        <BannerDetailPageLayout />
        <BodyDetailPageLayout />
      </MainLayout>
    </div>
  )
}

export default DetailPage;
