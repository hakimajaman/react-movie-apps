import React from 'react';
import MainLayout from '../layouts/MainLayout';
import BannerHomePageLayout from '../layouts/BannerHomePageLayout';
import BodyHomePageLayout from '../layouts/BodyHomePageLayout';

const HomePage = () => {
  return (
    <div>
      <MainLayout>
        <BannerHomePageLayout />
        <BodyHomePageLayout />
      </MainLayout>
    </div>
  )
}

export default HomePage;
