import React, { useEffect } from 'react';
import { Link } from 'react-router-dom'

//Redux
import { connect } from 'react-redux';
import { getAllMovies, getMovie } from '../store/actions/movies';

const BodyHomePageComponent = (props) => {

  const images = [
    require('../assets/images/yakusoku-no-neverland.png'),
    require('../assets/images/kimetsu-no-yaiba-1.jpg'),
    require('../assets/images/kimi-no-nawa-1.jpg'),
    require('../assets/images/kimi-ni-todoke.jpg'),
    require('../assets/images/take-my-brother-away.jpg')
  ]

  const seeMore = () => {
    props.getMovie()
  }

  const allMovies = () => {
    if(props.allMovies.movieReducers.movies.docs !== undefined) {
      return(
        props.allMovies.movieReducers.movies.docs.map(item =>
          (
            <div className="col" key={item._id}>
              <div className="col-hover">
                <img src={item.poster} alt="img1" className="movie-image" />
                <div className="col-button">
                  <p id="textmovies-button" onClick={() => seeMore(item._id)}><Link to={`/list/overview/${item._id}`}>See More</Link></p>
                  <p id="textmovies-button">Watch Trailer</p>
                </div>
              </div>
              <div className="col-text">
                <p>{item.title}</p>
                <span>{item.categories.join(",  ")}</span>
              </div>
            </div>
          )
        )
      )
    }
  }

  useEffect(() => {
    props.getAllMovies()
  }, [])

  console.log(props.allMovies)
  return (
    <div className="body-container__images">
      {allMovies()}
    </div>
  )
}

const mapStateToProps = (state) => {
  return({
    allMovies: state
  })
}

export default connect(mapStateToProps, {
  getAllMovies,
  getMovie
})(BodyHomePageComponent);
