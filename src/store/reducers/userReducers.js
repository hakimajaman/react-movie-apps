import {
  POST_REGISTER_USER,
  POST_LOGIN_USER,
  GET_CURRENT_USER,
  UPDATE_MY_PROFILE
} from '../actions/types';

const initialState = {
  users: {},
};

const userReducers = (state = initialState, action) => {
  const {type, payload} = action;
  switch (type) {
    case POST_LOGIN_USER || POST_REGISTER_USER:
      localStorage.setItem('userAccount', payload.data);
      return {
        ...state,
        users: {payload},
      };
    case GET_CURRENT_USER:
      return {
        ...state,
        users: {payload},
      };
    case UPDATE_MY_PROFILE:
      return {
        ...state,
        users: {payload}
      }
    default:
      return {
        ...state,
      };
  }
};

export default userReducers;
