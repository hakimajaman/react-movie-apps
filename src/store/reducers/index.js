import { combineReducers } from 'redux';
import movieReducers from './movieReducers';
import userReducers from './userReducers';
import reviewReducers from './reviewReducers';

export default combineReducers({
  movieReducers,
  userReducers,
  reviewReducers
});
