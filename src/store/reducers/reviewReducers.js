import { GET_ALL_REVIEWS, POST_REVIEW } from '../actions/types';

const initialState = {
  reviews: []
}

const reviewReducers = (state = initialState, action) => {
  const { type, payload } = action
  switch(type) {
    case GET_ALL_REVIEWS:
      return {
        ...state,
        reviews: payload
      }
    case POST_REVIEW:
      return {
        ...state,
        reviews: [payload, ...state]
      }
    default :
      return {
        ...state
      }
  }
}

export default reviewReducers;
