import {
  GET_ALL_MOVIES,
  GET_MOVIE,
  SEARCH_MOVIES,
  SEARCH_TAG_MOVIES,
  SEARCH_CATEGORY_MOVIES,
} from '../actions/types';

const initialState = {
  movies: [],
  movie: [],
  pagination: []
};

const movieReducers = (state = initialState, action) => {
  const {type, payload} = action;
  switch (type) {
    case GET_ALL_MOVIES:
      return {
        ...state,
        movies: payload,
      };
    case GET_MOVIE:
      return {
        ...state,
        movie: payload
      }
    case SEARCH_MOVIES:
      return {
        ...state,
        movies: payload,
      };
    case SEARCH_TAG_MOVIES:
      return {
        ...state,
        movies: payload,
      };
    case SEARCH_CATEGORY_MOVIES:
      return {
        ...state,
        movies: payload,
      };
    default:
      return {
        ...state,
      };
  }
};

export default movieReducers;
