import {
  POST_REGISTER_USER,
  POST_LOGIN_USER,
  GET_CURRENT_USER,
  UPDATE_MY_PROFILE,
  UPDATE_MY_PICTURE
} from './types';

const url = "https://reviewmoviedatabase.herokuapp.com/api/v1/users";

export const registerUser = (name, email, password) => async(dispatch) => {
  try {
    const thisData = {
      name: name,
      email: email,
      password: password
    }
    const send = await fetch(`${url}/register`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(thisData)
    })
    const res = await send.json()
    dispatch({
      type: POST_REGISTER_USER,
      payload: res
    })
  } catch(err) {
    dispatch({
      type: POST_REGISTER_USER,
      payload: err
    })
  }
}

export const loginUser = (email, password) => async(dispatch) => {
  try {
    const thisData = {
      email: email,
      password: password
    }
    const send = await fetch(`${url}/login`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(thisData)
    })
    const res = await send.json();
    dispatch({
      type: POST_LOGIN_USER,
      payload: res
    })
  } catch(err) {
    dispatch({
      type: POST_LOGIN_USER,
      payload: err
    })
  }
}

export const currentUser = () => async(dispatch) => {
  try {
    const send = await fetch(`${url}/currentUser`, {
      headers: {
        'Content-Type': 'application/json',
        Authorization: "Bearer "+localStorage.getItem('userAccount')
      }
    })
    const res = await send.json();
    dispatch({
      type: GET_CURRENT_USER,
      payload: res
    })
  } catch(err) {
    dispatch({
      type: GET_CURRENT_USER,
      payload: err
    })
  }
}

export const updateMyProfile = (name) => async(dispatch) => {
  try {
    const thisData = {
      name: name
    }
    const send = await fetch(`${url}/update`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        Authorization: "Bearer "+localStorage.getItem('userAccount')
      },
      body: JSON.stringify(thisData)
    })
    const res = await send.json();
    dispatch({
      type: UPDATE_MY_PROFILE,
      payload: res
    })
  } catch(err) {
    dispatch({
      type: UPDATE_MY_PROFILE,
      payload: err
    })
  }
}

export const updateMyPicture = (picture) => async(dispatch) => {
  try {
    const thisData = {
      image: picture
    }
    const send = await fetch(`${url}/uploadPhoto`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'multipart/form-data',
        Authorization: "Bearer "+localStorage.getItem('userAccount')
      },
      body: JSON.stringify(thisData)
    })
    const res = await send.json()
    dispatch({
      type: UPDATE_MY_PICTURE,
      payload: res
    })
  } catch(err) {
    dispatch({
      type: UPDATE_MY_PICTURE,
      payload: err
    })
  }
}
