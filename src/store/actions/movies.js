import {
  GET_ALL_MOVIES,
  GET_MOVIE,
  SEARCH_MOVIES,
  SEARCH_TAG_MOVIES,
  SEARCH_CATEGORY_MOVIES,
} from './types';

const url = 'https://reviewmoviedatabase.herokuapp.com/api/v1/movies';

export const getAllMovies = () => async dispatch => {
  try {
    const res = await fetch(`${url}/searchall?limit=15&page=1`);
    const data = await res.json();
    dispatch({
      type: GET_ALL_MOVIES,
      payload: data.data,
    });
  } catch (err) {
    dispatch({
      type: GET_ALL_MOVIES,
      payload: err,
    });
  }
};

export const getAllMoviesPagination = (page) => async dispatch => {
  try {
    const res = await fetch(`${url}/searchall?limit=15&page=${page}`);
    const data = await res.json();
    dispatch({
      type: GET_ALL_MOVIES,
      payload: data.data,
    });
  } catch(err) {
    dispatch({
      type: GET_ALL_MOVIES,
      payload: err
    })
  }
}

export const getMovie = id => async dispatch => {
  try {
    const res = await fetch(`${url}/id?id=${id}`);
    const data = await res.json();
    dispatch({
      type: GET_MOVIE,
      payload: data.data,
    });
  } catch (err) {
    dispatch({
      type: GET_MOVIE,
      payload: err,
    });
  }
};

export const searchMovies = search => async dispatch => {
  try {
    const res = await fetch(
      `${url}/searchall?limit=15&page=1&search=${search}`,
    );
    const data = await res.json();
    dispatch({
      type: SEARCH_MOVIES,
      payload: data.data.docs,
    });
  } catch (err) {
    dispatch({
      type: SEARCH_MOVIES,
      payload: err,
    });
  }
};

export const searchTagMovies = tag => async dispatch => {
  try {
    const res = await fetch(`${url}/searchall?limit=15&page=1&tag=${tag}`);
    const data = await res.json();
    dispatch({
      type: SEARCH_TAG_MOVIES,
      payload: data.data.docs,
    });
  } catch (err) {
    dispatch({
      type: SEARCH_TAG_MOVIES,
      payload: err,
    });
  }
};

export const searchCategoryMovies = category => async dispatch => {
  try {
    const res = await fetch(
      `${url}/searchall?limit=15&page=1&category=${category}`,
    );
    const data = await res.json();
    dispatch({
      type: SEARCH_CATEGORY_MOVIES,
      payload: data.data.docs,
    });
  } catch (err) {
    dispatch({
      type: SEARCH_CATEGORY_MOVIES,
      payload: err,
    });
  }
};
