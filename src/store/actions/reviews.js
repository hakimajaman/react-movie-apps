import { GET_ALL_REVIEWS, POST_REVIEW } from './types';

const url = 'https://reviewmoviedatabase.herokuapp.com/api/v1/reviews';

export const getAllReviews = (id) => async dispatch => {
  try {
    const res = await fetch(`${url}/beforelogin?movieId=${id}`)
    const data = await res.json();
    dispatch({
      type: GET_ALL_REVIEWS,
      payload: data.data
    })
  } catch(err) {
    dispatch({
      type: GET_ALL_REVIEWS,
      payload: err
    })
  }
}

export const postReview = (rating, review, movieId) => async dispatch => {
  try {
    const thisData = {
      rating: rating,
      review: review,
      movieId: movieId
    }
    const send = await fetch(`${url}`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: "Bearer "+localStorage.getItem('userAccount')
      },
      body: JSON.stringify(thisData)
    })
    const res = await send.json();
    console.log(res)
  } catch(err) {
    console.log(err)
    dispatch({
      type: POST_REVIEW,
      payload: err
    })
  }
}
