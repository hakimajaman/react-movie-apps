import React, { useEffect, useState } from 'react';
import logo from './logo.svg';
import './App.css';
import { BrowserRouter, Route, useHistory } from 'react-router-dom';
import "./assets/sass/App.scss";
import { library } from '@fortawesome/fontawesome-svg-core'
import { fas } from '@fortawesome/free-solid-svg-icons'

//Components and Pages
import HomePage from './components/pages/HomePage';
import ProfilePage from './components/ProfileComponent';
import DetailPage from './components/pages/DetailPage';
import DetailPageOverview from './components/pages/DetailOverviewPage';
import DetailPageCharacters from './components/pages/DetailCharactersPage';

library.add(fas);

const App = (props) => {
  return (
    <BrowserRouter className="appjs" style={{margin: 0}}>
      <Route path="/" exact>
        <HomePage/>
      </Route>
      <Route path="/profile" exact>
        <ProfilePage/>
      </Route>
      <Route path="/list/overview/:id" exact>
        <DetailPageOverview/>
      </Route>
      <Route path="/list/review/:id" exact>
        <DetailPage/>
      </Route>
      <Route path="/list/characters/:id" exact>
        <DetailPageCharacters />
      </Route>
    </BrowserRouter>
  );
}

export default App;
